## Cliente: Textiles Camir, C.A
## RIF: J-40179487
## Correo electrónico: moruro@hotmail.com
## Tiempo de Desarrollo: tres(3) meses aproximadamente

## Maquetacion del Frontend

## Maquetacion del Backend

### Frontend
	- Inicio(Bienvenida, Banner, Destacados, Clientes). *
	- Nosotros(Misión, Visión, etc). *
	- Productos *
		+ Categorias *
		+ Presupuesto * 
		+ Compra(Con estatus)
	- Contacto *
	- Registro *
	- Mi Cuenta *
		+ Mis Datos *
		+ Mis Compras

### Backend
	- Inicio *
	- Nosotros *
	- Modulo de Usuarios(CRUD) **
	- Clientes (CRUD) **
	- Productos (CRUD) **
	- Compras (RU)
	- Presupuesto
	- Configuracion **
	- Contacto **

### Redes Sociales
	- Facebook
	- Twitter
	- Instagram
	- Integración de las redes sociales con la aplicación

### Servicio de Mantenimiento y respaldo 



